/* Copyright (C) 2021-2024 Marco Scarpetta
 *
 * This file is part of PDF Mix Tool.
 *
 * PDF Mix Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PDF Mix Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PDF Mix Tool. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EDITDOCUMENTINFO_H
#define EDITDOCUMENTINFO_H

#include <QLineEdit>
#include <QDateTimeEdit>
#include <QCheckBox>

#include "abstract_operation.h"

class EditDocumentInfo : public AbstractOperation
{
    Q_OBJECT
public:
    explicit EditDocumentInfo(QWidget *parent = nullptr);

    void set_pdf_info(const PdfInfo &pdf_info) override;

    int output_pages_count() override;

private:
    QLineEdit m_title;
    QLineEdit m_author;
    QLineEdit m_subject;
    QLineEdit m_keywords;
    QLineEdit m_creator;
    QLineEdit m_producer;
    QDateTimeEdit m_creation_date;
    QDateTimeEdit m_mod_date;
    QCheckBox m_creation_date_enabled;
    QCheckBox m_mod_date_enabled;

    void save(bool save_as);
};

#endif // EDITDOCUMENTINFO_H
