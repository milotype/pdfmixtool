<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hi">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>PDF Mix Tool के बारे में</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="41"/>
        <source>Close</source>
        <translation>बंद करे</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="51"/>
        <source>Version %1</source>
        <translation>संस्करण %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="64"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>PDF फाइलों पर सामान्य संपादन कार्य करने के लिए एप्लिकेशन।</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>Website</source>
        <translation>वेबसाइट</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="74"/>
        <source>About</source>
        <translation>बारे में</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Translators</source>
        <translation>अनुवादक</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="98"/>
        <source>Credits</source>
        <translation>श्रेय</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="122"/>
        <source>License</source>
        <translation>लाइसेंस</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="135"/>
        <source>Submit a pull request</source>
        <translation>पुल अनुरोध सबमिट करें</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="136"/>
        <source>Report a bug</source>
        <translation>बग की रिपोर्ट करें</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Help translating</source>
        <translation>अनुवाद करने में सहायता करें</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Contribute</source>
        <translation>योगदान करें</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="160"/>
        <source>Changelog</source>
        <translation>चेंजलॉग</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="41"/>
        <source>Save</source>
        <translation>सहेजें</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="102"/>
        <source>Overwrite File?</source>
        <translation>फ़ाइल अधिलेखित करें?</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="103"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>&apos;&apos;%1&apos;&apos; नामक फाइल पहले से मौजूद है। क्या आप इसे अधिलेखित करना चाहते हैं?</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="109"/>
        <source>Always overwrite</source>
        <translation>हमेशा अधिलेखित करें</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="127"/>
        <source>Save PDF file</source>
        <translation>PDF फाइल सेव करें</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="131"/>
        <source>PDF files</source>
        <translation>PDF फाइलें</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="32"/>
        <source>Add empty pages</source>
        <translation>खाली पन्ने जोड़ें</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="45"/>
        <source>Count:</source>
        <translation>गिनती:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="52"/>
        <source>Page size</source>
        <translation>पृष्ठ आकार</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="55"/>
        <source>Same as document</source>
        <translation>दस्तावेज़ के समान</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="59"/>
        <source>Custom:</source>
        <translation>कस्टम:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="85"/>
        <source>Standard:</source>
        <translation>मानक:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="95"/>
        <source>Portrait</source>
        <translation>पोर्ट्रेट</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="97"/>
        <source>Landscape</source>
        <translation>लैंडस्केप</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="100"/>
        <source>Location</source>
        <translation>स्थान</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="103"/>
        <source>Before</source>
        <translation>पहले</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="106"/>
        <source>After</source>
        <translation>बाद</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="110"/>
        <source>Page:</source>
        <translation>पेज:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="124"/>
        <source>Save as…</source>
        <translation>ऐसे सहेजें…</translation>
    </message>
</context>
<context>
    <name>AlternateMix</name>
    <message>
        <location filename="../src/operations/alternate_mix.cpp" line="26"/>
        <source>Alternate mix</source>
        <translation>वैकल्पिक मिश्रण</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/operations/booklet.cpp" line="31"/>
        <source>Booklet</source>
        <translation>पुस्तिका</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="41"/>
        <source>Left</source>
        <translation>बाएं</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="42"/>
        <source>Right</source>
        <translation>दाएं</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="43"/>
        <source>Binding:</source>
        <translation>बंधन:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="44"/>
        <source>Use last page as back cover:</source>
        <translation>अंतिम पृष्ठ को बैक कवर के रूप में उपयोग करें:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="46"/>
        <source>Generate two booklets in one sheet:</source>
        <translation>एक शीट में दो पुस्तिकाएँ बनाएँ:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="54"/>
        <source>Generate booklet</source>
        <translation>पुस्तिका तैयार करें</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="75"/>
        <source>Save booklet PDF file</source>
        <translation>बुकलेट PDF फाइल सेव करें</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="79"/>
        <source>PDF files</source>
        <translation>PDF फाइलें</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="32"/>
        <source>Delete pages</source>
        <translation>पन्ने हटाएँ</translation>
    </message>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="56"/>
        <source>Save as…</source>
        <translation>ऐसे सहेजें…</translation>
    </message>
</context>
<context>
    <name>EditDocumentInfo</name>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="14"/>
        <source>Document information</source>
        <translation>दस्तावेज़ जानकारी</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="24"/>
        <source>Title:</source>
        <translation>शीर्षक:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="27"/>
        <source>Author:</source>
        <translation>लेखक:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="30"/>
        <source>Subject:</source>
        <translation>विषय:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="33"/>
        <source>Keywords:</source>
        <translation>कीवर्ड:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="36"/>
        <source>Creator:</source>
        <translation>निर्माता:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="39"/>
        <source>Producer:</source>
        <translation>निर्माता:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="42"/>
        <source>Creation date:</source>
        <translation>निर्माण तिथि:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="46"/>
        <source>Modification date:</source>
        <translation>संशोधन तिथि:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="75"/>
        <source>Save as…</source>
        <translation>इस रूप में सहेजें…</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>मल्टीपेज प्रोफ़ाइल संपादित करें</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="37"/>
        <source>Name:</source>
        <translation>नाम:</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="34"/>
        <source>Pages layout</source>
        <translation>पृष्ठों का लेआउट</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="44"/>
        <source>Apply to:</source>
        <translation>इन पर लागू करें:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="70"/>
        <source>Save as…</source>
        <translation>इस रूप में सहेजें…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>PDF फाइलों की प्रॉपर्टी को संपादित करें</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>कोई घुमाव नहीं</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>अक्षम</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>ठीक है</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>रद्द</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>बहुपृष्ठ:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>घूर्णन:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="35"/>
        <source>Extract pages</source>
        <translation>पन्ने निकालें</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="51"/>
        <source>Extract to individual PDF files</source>
        <translation>अलग-अलग PDF फाइलों में निकालें</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="55"/>
        <source>Output PDF base name:</source>
        <translation>आउटपुट PDF आधार नाम:</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="64"/>
        <location filename="../src/operations/extract_pages.cpp" line="79"/>
        <source>Extract…</source>
        <translation>निकालें…</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="74"/>
        <location filename="../src/operations/extract_pages.cpp" line="174"/>
        <source>Extract to single PDF</source>
        <translation>एकल PDF में निकालें</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="116"/>
        <source>Select save directory</source>
        <translation>सहेजने के लिए डायरेक्टरी का चयन करें</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="178"/>
        <source>PDF files</source>
        <translation>PDF फाइलें</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="81"/>
        <source>Page order:</source>
        <translation>पृष्ठ क्रम:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="84"/>
        <source>reverse</source>
        <translation>उल्टा</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="86"/>
        <source>forward</source>
        <translation>आगे</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="109"/>
        <source>All</source>
        <translation>सभी</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="122"/>
        <source>Pages:</source>
        <translation>पन्ने:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="125"/>
        <source>Multipage:</source>
        <translation>बहुपृष्ठ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="130"/>
        <source>Disabled</source>
        <translation>अक्षम</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="133"/>
        <source>Rotation:</source>
        <translation>घूर्णन:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="136"/>
        <source>Outline entry:</source>
        <translation>रूपरेखा प्रविष्टि:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="60"/>
        <source>Reverse page order:</source>
        <translation>उलटा पृष्ठ क्रम:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="74"/>
        <source>Disabled</source>
        <translation>अक्षम</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="82"/>
        <source>New custom profile…</source>
        <translation>नई कस्टम प्रोफ़ाइल…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="84"/>
        <source>No rotation</source>
        <translation>कोई घुमाव नहीं</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="89"/>
        <source>Pages:</source>
        <translation>पन्ने:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="91"/>
        <source>Multipage:</source>
        <translation>बहुपृष्ठ:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="93"/>
        <source>Rotation:</source>
        <translation>घूर्णन:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="95"/>
        <source>Outline entry:</source>
        <translation>रूपरेखा प्रविष्टि:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="88"/>
        <source>Menu</source>
        <translation>मेन्यू</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="93"/>
        <source>Multipage profiles…</source>
        <translation>बहुपृष्ठ प्रोफाइल…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>About</source>
        <translation>बारे में</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="103"/>
        <source>Exit</source>
        <translation>बाहर निकलें</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Error generating the PDF</source>
        <translation>PDF जनरेट करने में त्रुटि</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Open PDF file…</source>
        <translation>PDF फाइल खोलें…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="292"/>
        <source>PDF files</source>
        <translation>PDF फाइलें</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Error opening file</source>
        <translation>फाइल खोलने में त्रुटि</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="332"/>
        <source>Output pages: %1</source>
        <translation>आउटपुट पेजः %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="290"/>
        <source>Select a PDF file</source>
        <translation>एक PDF फाइल चुनें</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Files saved in %1.</source>
        <translation>फाइलें %1 में सहेजी गईं।</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="368"/>
        <source>File %1 saved.</source>
        <translation>फाइल %1 सहेजी गयी।</translation>
    </message>
</context>
<context>
    <name>Merge</name>
    <message>
        <location filename="../src/operations/merge.cpp" line="42"/>
        <source>Merge PDF files</source>
        <translation>PDF फाइलों को मर्ज करें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="70"/>
        <source>Edit</source>
        <translation>संपादन</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="71"/>
        <source>View</source>
        <translation>देखें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="74"/>
        <source>Main toolbar</source>
        <translation>मुख्य टूलबार</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="81"/>
        <source>Add PDF file</source>
        <translation>PDF फाइल जोड़ें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="86"/>
        <source>Select one or more PDF files to open</source>
        <translation>खोलने के लिए एक या अधिक PDF फाइलों का चयन करें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="88"/>
        <location filename="../src/operations/merge.cpp" line="589"/>
        <source>PDF files</source>
        <translation>PDF फाइलें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="93"/>
        <source>Move up</source>
        <translation>ऊपर ले जाएँ</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="98"/>
        <source>Move down</source>
        <translation>नीचे ले जाएँ</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="103"/>
        <source>Remove file</source>
        <translation>फाइल हटाएँ</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="113"/>
        <source>Load files list</source>
        <translation>फाइलें सूची लोड करें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="118"/>
        <source>Save files list</source>
        <translation>फाइलें सूची सहेजें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="152"/>
        <location filename="../src/operations/merge.cpp" line="156"/>
        <source>Generate PDF</source>
        <translation>PDF जनरेट करें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="332"/>
        <source>Select the JSON file containing the files list</source>
        <translation>फाइलों की सूची वाली JSON फाइल का चयन करें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="334"/>
        <location filename="../src/operations/merge.cpp" line="352"/>
        <source>JSON files (*.json)</source>
        <translation>JSON फाइलें (*.json)</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="340"/>
        <source>Error while reading the JSON file!</source>
        <translation>JSON फाइल पढ़ते समय त्रुटि!</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="341"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation>JSON फाइल पढ़ते समय एक त्रुटि उत्पन्न हुई!</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="348"/>
        <source>Select a JSON file</source>
        <translation>एक JSON फाइल चुनें</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="427"/>
        <source>Error opening file</source>
        <translation>फाइल खोलने में त्रुटि</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="561"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;फाइल &lt;b&gt;%1&lt;/b&gt; के आउटपुट पृष्ठ ख़राब तरीके से स्वरूपित हैं। कृपया सुनिश्चित करें कि आपने निम्नलिखित नियमों का अनुपालन किया है:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;पृष्ठों के अंतराल को पहले पृष्ठ और अंतिम पृष्ठ को डैश से अलग करते हुए लिखा जाना चाहिए (उदाहरण के लिए &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;एकल पृष्ठ और पृष्ठों के अंतराल को रिक्त स्थान, अल्पविराम या दोनों से अलग किया जाना चाहिए (उदाहरण के लिए &quot;1, 2, 3, 5-10&quot; या &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt; सभी पृष्ठ और पृष्ठों का अंतराल 1 और PDF फाइल के पृष्ठों की संख्या के बीच होना चाहिए;&lt;/li&gt;&lt;li&gt;केवल संख्याएं, रिक्त स्थान, अल्पविराम और डैश का उपयोग किया जा सकता है। अन्य सभी वर्णों की अनुमति नहीं है।&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="578"/>
        <source>PDF generation error</source>
        <translation>PDF जनरेशन त्रुटि</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="585"/>
        <source>Save PDF file</source>
        <translation>PDF फाइल सेव करें</translation>
    </message>
</context>
<context>
    <name>MultipageEditor</name>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="34"/>
        <source>Standard size</source>
        <translation>मानक आकार</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="35"/>
        <source>Custom size</source>
        <translation>प्रचलन आकार</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="44"/>
        <source>Portrait</source>
        <translation>पोर्ट्रेट</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="45"/>
        <source>Landscape</source>
        <translation>लैंडस्केप</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="65"/>
        <source>Right-to-left</source>
        <translation>दाएं से बाएं</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="67"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="202"/>
        <source>Left</source>
        <translation>बाएं</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="68"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="72"/>
        <source>Center</source>
        <translation>केंद्र</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="69"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="205"/>
        <source>Right</source>
        <translation>दाएं</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="71"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="208"/>
        <source>Top</source>
        <translation>शीर्ष</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="73"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="211"/>
        <source>Bottom</source>
        <translation>तल</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="106"/>
        <source>Page size</source>
        <translation>पृष्ठ आकार</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="125"/>
        <source>Orientation:</source>
        <translation>ओरिएंटेशन:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="132"/>
        <source>Width:</source>
        <translation>चौड़ाई:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="135"/>
        <source>Height:</source>
        <translation>ऊंचाई:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="162"/>
        <source>Pages layout</source>
        <translation>पृष्ठों का लेआउट</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="169"/>
        <source>Rows:</source>
        <translation>पंक्तियाँ:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="172"/>
        <source>Columns:</source>
        <translation>कॉलम:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="175"/>
        <source>Spacing:</source>
        <translation>रिक्ति:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="181"/>
        <source>Pages alignment</source>
        <translation>पृष्ठों का संरेखण</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="188"/>
        <source>Horizontal:</source>
        <translation>क्षैतिज:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="191"/>
        <source>Vertical:</source>
        <translation>लंबवत:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="195"/>
        <source>Margins</source>
        <translation>मार्जिन</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>नई प्रोफ़ाइल…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>प्रोफ़ाइल हटाएं</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="40"/>
        <source>Manage multipage profiles</source>
        <translation>बहुपृष्ठ प्रोफ़ाइल प्रबंधित करें</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="72"/>
        <source>Edit profile</source>
        <translation>प्रोफ़ाइल संपादन</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="118"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="120"/>
        <source>Custom profile</source>
        <translation>कस्टम प्रोफ़ाइल</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="179"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="202"/>
        <source>Error</source>
        <translation>त्रुटि</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="180"/>
        <source>Profile name can not be empty.</source>
        <translation>प्रोफ़ाइल नाम रिक्त नहीं हो सकता।</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="185"/>
        <source>Disabled</source>
        <translation>अक्षम</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="203"/>
        <source>Profile name already exists.</source>
        <translation>प्रोफ़ाइल नाम पहले से मौजूद है।</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Pages:</source>
        <translation>पन्ने:</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="47"/>
        <source>Even pages</source>
        <translation>सम पृष्ठ</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>Odd pages</source>
        <translation>विषम पृष्ठ</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="50"/>
        <source>All pages</source>
        <translation>सभी पन्ने</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="125"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;पेज अंतराल बुरी तरह से स्वरूपित हैं। कृपया सुनिश्चित करें कि आपने निम्नलिखित नियमों का अनुपालन किया है:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;पृष्ठों के अंतराल को पहले पृष्ठ और अंतिम पृष्ठ को डैश से अलग करते हुए लिखा जाना चाहिए (उदाहरण के लिए &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;एकल पृष्ठ और पृष्ठों के अंतराल को रिक्त स्थान, अल्पविराम या दोनों से अलग किया जाना चाहिए (उदाहरण के लिए &quot;1, 2, 3, 5-10&quot; या &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt; सभी पृष्ठ और पृष्ठों का अंतराल 1 और PDF फाइल के पृष्ठों की संख्या के बीच होना चाहिए;&lt;/li&gt;&lt;li&gt;केवल संख्याएं, रिक्त स्थान, अल्पविराम और डैश का उपयोग किया जा सकता है। अन्य सभी वर्णों की अनुमति नहीं है।&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="141"/>
        <source>Error</source>
        <translation>त्रुटि</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>पोर्ट्रेट</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>लैंडस्केप</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n पेज</numerusform>
            <numerusform>%n पन्ने</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Rotate</name>
    <message>
        <location filename="../src/operations/rotate.cpp" line="32"/>
        <source>Rotate</source>
        <translation>घुमाएँ</translation>
    </message>
    <message>
        <location filename="../src/operations/rotate.cpp" line="73"/>
        <source>Save as…</source>
        <translation>इस रूप में सहेजें…</translation>
    </message>
</context>
</TS>
