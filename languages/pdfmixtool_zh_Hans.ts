<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hans">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>关于 PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="41"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="51"/>
        <source>Version %1</source>
        <translation>版本 %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="64"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>对 PDF 文件执行常见编辑操作的应用程序。</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>Website</source>
        <translation>网站</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="74"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Translators</source>
        <translation>翻译人员</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="98"/>
        <source>Credits</source>
        <translation>鸣谢</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="122"/>
        <source>License</source>
        <translation>许可</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="135"/>
        <source>Submit a pull request</source>
        <translation>提交拉取请求</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="136"/>
        <source>Report a bug</source>
        <translation>报告错误</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Help translating</source>
        <translation>帮助翻译</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Contribute</source>
        <translation>贡献</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="160"/>
        <source>Changelog</source>
        <translation>变更记录</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="41"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="102"/>
        <source>Overwrite File?</source>
        <translation>覆盖文件？</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="103"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>名为 «%1» 的文件已存在。是否要覆盖它？</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="109"/>
        <source>Always overwrite</source>
        <translation>总是覆盖</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="127"/>
        <source>Save PDF file</source>
        <translation>保存 PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="131"/>
        <source>PDF files</source>
        <translation>PDF 文件</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="32"/>
        <source>Add empty pages</source>
        <translation>添加空页</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="45"/>
        <source>Count:</source>
        <translation>总数：</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="52"/>
        <source>Page size</source>
        <translation>页面大小</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="55"/>
        <source>Same as document</source>
        <translation>与文档相同</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="59"/>
        <source>Custom:</source>
        <translation>自定义：</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="85"/>
        <source>Standard:</source>
        <translation>标准：</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="95"/>
        <source>Portrait</source>
        <translation>纵向</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="97"/>
        <source>Landscape</source>
        <translation>横向</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="100"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="103"/>
        <source>Before</source>
        <translation>之前</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="106"/>
        <source>After</source>
        <translation>之后</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="110"/>
        <source>Page:</source>
        <translation>页面：</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="124"/>
        <source>Save as…</source>
        <translation>另存为…</translation>
    </message>
</context>
<context>
    <name>AlternateMix</name>
    <message>
        <location filename="../src/operations/alternate_mix.cpp" line="26"/>
        <source>Alternate mix</source>
        <translation>交替组合</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/operations/booklet.cpp" line="31"/>
        <source>Booklet</source>
        <translation>小册子</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="41"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="42"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="43"/>
        <source>Binding:</source>
        <translation>装订：</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="44"/>
        <source>Use last page as back cover:</source>
        <translation>使用最后一页作为封底：</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="46"/>
        <source>Generate two booklets in one sheet:</source>
        <translation>一页内生成两个小册子：</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="54"/>
        <source>Generate booklet</source>
        <translation>生成小册子</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="75"/>
        <source>Save booklet PDF file</source>
        <translation>保存小册子 PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="79"/>
        <source>PDF files</source>
        <translation>PDF 文件</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="32"/>
        <source>Delete pages</source>
        <translation>删除页面</translation>
    </message>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="56"/>
        <source>Save as…</source>
        <translation>保存为…</translation>
    </message>
</context>
<context>
    <name>EditDocumentInfo</name>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="14"/>
        <source>Document information</source>
        <translation>文档信息</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="24"/>
        <source>Title:</source>
        <translation>标题：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="27"/>
        <source>Author:</source>
        <translation>作者：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="30"/>
        <source>Subject:</source>
        <translation>主题：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="33"/>
        <source>Keywords:</source>
        <translation>关键词：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="36"/>
        <source>Creator:</source>
        <translation>创建者：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="39"/>
        <source>Producer:</source>
        <translation>出版商：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="42"/>
        <source>Creation date:</source>
        <translation>创建日期：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="46"/>
        <source>Modification date:</source>
        <translation>修改日期：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="75"/>
        <source>Save as…</source>
        <translation>另存为…</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>编辑多页配置文件</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="37"/>
        <source>Name:</source>
        <translation>名称：</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="34"/>
        <source>Pages layout</source>
        <translation>页面布局</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="44"/>
        <source>Apply to:</source>
        <translation>应用于：</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="70"/>
        <source>Save as…</source>
        <translation>另存为…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>编辑 PDF 文件的属性</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>无旋转</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>多页：</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>旋转：</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="35"/>
        <source>Extract pages</source>
        <translation>提取页面</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="51"/>
        <source>Extract to individual PDF files</source>
        <translation>提取到单独的PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="55"/>
        <source>Output PDF base name:</source>
        <translation>输出 PDF 基本名：</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="64"/>
        <location filename="../src/operations/extract_pages.cpp" line="79"/>
        <source>Extract…</source>
        <translation>提取…</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="74"/>
        <location filename="../src/operations/extract_pages.cpp" line="174"/>
        <source>Extract to single PDF</source>
        <translation>提取到单个 PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="116"/>
        <source>Select save directory</source>
        <translation>选择保存目录</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="178"/>
        <source>PDF files</source>
        <translation>PDF 文件</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="81"/>
        <source>Page order:</source>
        <translation>页面顺序：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="84"/>
        <source>reverse</source>
        <translation>反向</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="86"/>
        <source>forward</source>
        <translation>正向</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="109"/>
        <source>All</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="122"/>
        <source>Pages:</source>
        <translation>页面：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="125"/>
        <source>Multipage:</source>
        <translation>多页：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="130"/>
        <source>Disabled</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="133"/>
        <source>Rotation:</source>
        <translation>旋转：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="136"/>
        <source>Outline entry:</source>
        <translation>大纲条目：</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="60"/>
        <source>Reverse page order:</source>
        <translation>反向页面顺序：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="74"/>
        <source>Disabled</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="82"/>
        <source>New custom profile…</source>
        <translation>新的自定义配置文件…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="84"/>
        <source>No rotation</source>
        <translation>无旋转</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="89"/>
        <source>Pages:</source>
        <translation>页面：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="91"/>
        <source>Multipage:</source>
        <translation>多页：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="93"/>
        <source>Rotation:</source>
        <translation>旋转：</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="95"/>
        <source>Outline entry:</source>
        <translation>大纲条目：</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="88"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="93"/>
        <source>Multipage profiles…</source>
        <translation>多页配置文件…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="103"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Error generating the PDF</source>
        <translation>生成 PDF 时出错</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Open PDF file…</source>
        <translation>打开 PDF 文件…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="292"/>
        <source>PDF files</source>
        <translation>PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Error opening file</source>
        <translation>打开文件失败</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="332"/>
        <source>Output pages: %1</source>
        <translation>输出页数： %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="290"/>
        <source>Select a PDF file</source>
        <translation>选择 PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Files saved in %1.</source>
        <translation>文件保存在 %1 中。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="368"/>
        <source>File %1 saved.</source>
        <translation>文件 %1 已保存。</translation>
    </message>
</context>
<context>
    <name>Merge</name>
    <message>
        <location filename="../src/operations/merge.cpp" line="42"/>
        <source>Merge PDF files</source>
        <translation>合并 PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="70"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="71"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="74"/>
        <source>Main toolbar</source>
        <translation>主工具栏</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="81"/>
        <source>Add PDF file</source>
        <translation>添加 PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="86"/>
        <source>Select one or more PDF files to open</source>
        <translation>选择要打开的一个或多个 PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="88"/>
        <location filename="../src/operations/merge.cpp" line="589"/>
        <source>PDF files</source>
        <translation>PDF 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="93"/>
        <source>Move up</source>
        <translation>上移</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="98"/>
        <source>Move down</source>
        <translation>下移</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="103"/>
        <source>Remove file</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="113"/>
        <source>Load files list</source>
        <translation>加载文件列表</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="118"/>
        <source>Save files list</source>
        <translation>保存文件列表</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="152"/>
        <location filename="../src/operations/merge.cpp" line="156"/>
        <source>Generate PDF</source>
        <translation>生成 PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="332"/>
        <source>Select the JSON file containing the files list</source>
        <translation>选择包含文件列表的 JSON 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="334"/>
        <location filename="../src/operations/merge.cpp" line="352"/>
        <source>JSON files (*.json)</source>
        <translation>JSON 文件 (*.json)</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="340"/>
        <source>Error while reading the JSON file!</source>
        <translation>读取 JSON 文件时出错！</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="341"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation>读取 JSON 文件时发生了错误！</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="348"/>
        <source>Select a JSON file</source>
        <translation>选择 JSON 文件</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="427"/>
        <source>Error opening file</source>
        <translation>打开文件时出错</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="561"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;文件 &lt;b&gt;%1&lt;/b&gt; 的输出页格式不正确。请确保您遵守以下规则：&lt;/p&gt;&lt;ul&gt;&lt;li&gt;页面范围必须有起始页和最后一页，用连字符分隔（例如 &quot;1-5&quot;）；&lt;/li&gt;&lt;li&gt;单页和页面范围间必须用空格、逗号或两者分隔（例如 &quot;1, 2, 3, 5-10&quot; 或 &quot;1 2 3 5-10&quot;）；&lt;/li&gt;&lt;li&gt;所有页码和范围必须介于 1 与 PDF 文件页数之间；&lt;/li&gt;&lt;li&gt;只能使用数字、空格、英文逗号和连字符。不允许使用其他任何字符。&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="578"/>
        <source>PDF generation error</source>
        <translation>PDF 生成错误</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="585"/>
        <source>Save PDF file</source>
        <translation>保存 PDF 文件</translation>
    </message>
</context>
<context>
    <name>MultipageEditor</name>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="34"/>
        <source>Standard size</source>
        <translation>标准大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="35"/>
        <source>Custom size</source>
        <translation>自定义大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="44"/>
        <source>Portrait</source>
        <translation>纵向</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="45"/>
        <source>Landscape</source>
        <translation>横向</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="65"/>
        <source>Right-to-left</source>
        <translation>从右到左</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="67"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="202"/>
        <source>Left</source>
        <translation>左侧</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="68"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="72"/>
        <source>Center</source>
        <translation>居中</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="69"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="205"/>
        <source>Right</source>
        <translation>右侧</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="71"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="208"/>
        <source>Top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="73"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="211"/>
        <source>Bottom</source>
        <translation>底部</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="106"/>
        <source>Page size</source>
        <translation>页面大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="125"/>
        <source>Orientation:</source>
        <translation>方向：</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="132"/>
        <source>Width:</source>
        <translation>宽度：</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="135"/>
        <source>Height:</source>
        <translation>高度：</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="162"/>
        <source>Pages layout</source>
        <translation>页面布局</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="169"/>
        <source>Rows:</source>
        <translation>行:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="172"/>
        <source>Columns:</source>
        <translation>列：</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="175"/>
        <source>Spacing:</source>
        <translation>间距：</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="181"/>
        <source>Pages alignment</source>
        <translation>页面对齐</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="188"/>
        <source>Horizontal:</source>
        <translation>水平：</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="191"/>
        <source>Vertical:</source>
        <translation>垂直：</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="195"/>
        <source>Margins</source>
        <translation>边距</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>新配置文件…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>删除配置文件</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="40"/>
        <source>Manage multipage profiles</source>
        <translation>管理多页配置文件</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="72"/>
        <source>Edit profile</source>
        <translation>编辑配置文件</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="118"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="120"/>
        <source>Custom profile</source>
        <translation>自定义配置文件</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="179"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="202"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="180"/>
        <source>Profile name can not be empty.</source>
        <translation>配置文件名称不能为空。</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="185"/>
        <source>Disabled</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="203"/>
        <source>Profile name already exists.</source>
        <translation>配置文件名称已存在。</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Pages:</source>
        <translation>页面：</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="47"/>
        <source>Even pages</source>
        <translation>偶数页</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>Odd pages</source>
        <translation>奇数页</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="50"/>
        <source>All pages</source>
        <translation>所有页面</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="125"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;页面范围的格式不正确。请确保您遵守以下规则：&lt;/p&gt;&lt;ul&gt;&lt;li&gt;页面范围必须有起始页和最后一页，用连字符分隔（例如 &quot;1-5&quot;）；&lt;/li&gt;&lt;li&gt;单页和页面范围间必须用空格、逗号或两者分隔（例如 &quot;1, 2, 3, 5-10&quot; 或 &quot;1 2 3 5-10&quot;）；&lt;/li&gt;&lt;li&gt;所有页码和范围必须介于 1 与 PDF 文件页数之间；&lt;/li&gt;&lt;li&gt;只能使用数字、空格、英文逗号和连字符。不允许使用其他任何字符。&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="141"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>竖式</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>横式</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n 页</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Rotate</name>
    <message>
        <location filename="../src/operations/rotate.cpp" line="32"/>
        <source>Rotate</source>
        <translation>旋转</translation>
    </message>
    <message>
        <location filename="../src/operations/rotate.cpp" line="73"/>
        <source>Save as…</source>
        <translation>另存为…</translation>
    </message>
</context>
</TS>
